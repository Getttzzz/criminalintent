package com.example.getz.criminalintent;

import android.content.Context;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Getz on 23.12.2015.
 */
public class CrimeLab {

    private static CrimeLab sCrimeLab;
    private Context mContext;
    private ArrayList<Crime> mCrimes;

    private CrimeLab(Context pContext){
        mContext = pContext;
        mCrimes = new ArrayList<>();

        for (int i = 0; i < 100; i++) {
            Crime crime = new Crime();
            crime.setTitle("Crime #" + i);
            crime.setSolved(i%2==0);
            mCrimes.add(crime);
        }
    }

    public static CrimeLab get(Context pContext) {
        if(sCrimeLab == null){
            sCrimeLab = new CrimeLab(pContext.getApplicationContext());
        }
        return sCrimeLab;
    }

    public ArrayList<Crime> getCrimes() {
        return mCrimes;
    }

    public Crime getCrime(UUID id) {
        for (Crime c : mCrimes) {
            if (c.getId().equals(id))
                return c;
        }
        return null;
    }

}
