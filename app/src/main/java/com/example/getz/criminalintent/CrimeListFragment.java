package com.example.getz.criminalintent;

import android.app.ListFragment;
import android.os.Bundle;

import java.util.ArrayList;

public class CrimeListFragment extends ListFragment {

    private ArrayList<Crime> mCrimes;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle("Crimes");
        mCrimes = CrimeLab.get(getActivity()).getCrimes();
    }
}
